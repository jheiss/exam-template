# Using the D-MATH exam template

*Please note: Advanced features (e.g. randomization of MC questions, automatized printing of door signs and exam enrollment) are supported by the template. The documentation is in the process of being updated. Feel free to reach out if you have an efficient way to include these in other forms.*
    
*If you have specific questions / suggestions for improvement please direct them to your Group Organizer or [rc@math.ethz.ch](mailto:rc@math.ethz.ch).*

Tools for automation (linked below):

- Tool to generate anonymized list of students. 
- Tool to randomize exam questions with TeX.
- D-MATH exam print tool.
- Tool to send personalized eMail with exam number (coming in the future).
- Statistical analysis of multiple choice results (coming in the future).

**All exam related tools, instructions and links: [here](https://www2.math.ethz.ch/exam-information/exam-tools).**

## Download the repository

You can download the zipped D-MATH exam template [here](https://gitlab.math.ethz.ch/teaching/exam-template/-/archive/main/exam-template-main.zip). Alternatively: On the top right, hit the button to the left of 'clone', then download as a zip file. You may also clone the repo. 

## Files

D-MATH exams consist of two to three personalized documents per student, depending on whether the exam possesses a Multiple Choice (MC) part or not. 
**The exam template is modular. Depending on the type of exam you will need the following files.**
You can download the zipped template containing a complete sample exam with sample students and lecture infos [here](https://gitlab.math.ethz.ch/teaching/exam-template). Modify the questions for your exam based on the example and the documentation.

### TeX files for the exam:

##### If MC is present:

1. The exam itself `exam.tex`. [Example](https://gitlab.math.ethz.ch/teaching/exam-template/-/blob/main/examples/exam.pdf)
1. The MC answer sheet on which students fill boxes `answer_mc.tex`. [Example](https://gitlab.math.ethz.ch/teaching/exam-template/-/blob/main/examples/answer_mc.pdf)
1. The answer booklet in which students write their solutions for graded open questions `answer_book.tex`. [Example](https://gitlab.math.ethz.ch/teaching/exam-template/-/blob/main/examples/answer_book.pdf)

**No MC is present:** Then you only need files 1. and 3. above. 

**Pure MC exam:** Then you only need files 1. and 2. above.

The directory is pre-configured for use with the AMC software (run auto-multiple-choice from the terminal, choose 'open existing project', and navigate to the directly you downloaded. AMC should recognize it as a valid project. How-to is documented [here](https://gitlab.math.ethz.ch/teaching/amc-howto) (will be updated soon)).

### Input student and exam data:

To compile these files **you need to generate the following files with exam specific data**: 


- The file `registration.csv` containing student data. Use this [**tool to automatically generate the file**](https://isg.math.ethz.ch/make-anonym-csv/) upon downloading the excel file from eDoz with the list of examinees. Or follow [manual instructions](/readme_generate_csv.txt) (not recommended).
- The file `lectureinfos.sty` containing exam info. This information should be copied from eDoz and the Course Catalogue (Vorlesungsverzeichnis).

### Additional standard documents:
In addition the there is TeX code available for standard documents relating to exams:

- Door signs and attendance lists `lists_doorsigns.tex`. [Example](https://gitlab.math.ethz.ch/teaching/exam-template/-/blob/main/list_doorsigns.pdf). This file compiles using the `registration.csv` and `lectureinfos.sty` data.
- Folder labeling for archiving exams `folders.tex`. [Example](https://gitlab.math.ethz.ch/teaching/exam-template/-/blob/main/folders.pdf).



#### Grading file:
The file `GradingWorkbookCreator.xlsm` is an Excel based grading application, communicating with eDoz. See [below](https://gitlab.math.ethz.ch/teaching/exam-template/-/tree/main#grading) for details. 

## Working with the files

The sample exam is self-explanatory. Documentation for template-specific commands is appears in the TeX files as comment. e.g. 

> 
> %%% ---------- Controlling file output ---------- %%%
> 
> % 1 %% set language (0: german, 1: english)
> \def\english{0}
> 
> % 2 %% during the creation of the exam and for archiving it might be helpful to only compile one exam instead of all the exams for all students (for the solutions this option is ignored and always only one solution for Group v0 will be produced). To get one blank exam (i.e. without name, legi number etc) set both to 1
> \def\onlyOneModelExam{0} 	%set to 0 for printing all the exams 
> % \def\blankModelExam{0} 	%set to 1 for generating a model exam with blanks (also set \onlyOneModelExam to 1)
> 
> % 3 %% set depending on whether you want to compile the exam or its solution:
> 
> %%% compile solution (0: False, 1: True)
> \def\gensolution{0}
> 
> %%% show detailed points in the solution (only for correction, not to be given to students) (0: False, 1: True)
> \def\showpoints{1}
> 
> %%% if you don't want to use randomization via different groups set the following to 1 to compile all exams from Q0x_v0.tex
> % \def\noversions{1}
> 
> % 4 %% define the header and exam infos in lectureInfos.sty
> 
> % 5 %% BEFORE PRINTING CHECK:
> % - Are the infos in lectureInfos.sty correct (e.g., the date)?
> % - Do the point of the exercises match those on the cover page?
> % - Is their sum correct?
> % - Is the number of problems correct, does the numbering on the exam and the multiple choice answer sheet agree with that in the solution booklet?
> 
> % 6 %% Compile and prepare files for print using the D-MATH exam tool.
> % Check the documentation online https://gitlab.math.ethz.ch/amc/d-math-exam-template or the readme.md file.
> 

> %%% ---------- List of commands ----------------- %%%
> 
> % \itemMC : For multiple choice questions. Enumerate environment giving the format x.MCy (x=Problem, y=Subproblem)
> 
> % \itemOpen : For problems requiring written answers in the booklet. Enumerate environment giving the format x.Ay (german, Aufgabe) or x.Qy (english, Question), (x=Problem, y=Subproblem)
> 
> % \pointsA following \itemOpen prints the point for the subproblem. 
> 
> % \sol{ ... } solution of the problem. Will be printed when toggling the settings\gensolution
> 
> % \pointsM prints detailed points in the grading scheme. Will be shown when setting \showpoints{1}

## 1. Writing the exam: Compiling files & advanced features

Once you have written the exam questions and solutions, generate the multiple-choice answer sheet (if needed).

At this time you will also need to **generate the list of enrolled students**. For this, you can use [this online tool](https://isg.math.ethz.ch/make-anonym-csv/). In case you wish to randomize the order in which MC answers appear, check the "Randomization with TeX" section.

### LaTeX and auto-multiple-choice

The files `exam.tex` and `answer_book.tex` compile using regular LaTeX. The file `answer_mc.tex` must be compiled using Auto-Multiple-Choice. 

*Hint:* In case you insert TikZ plots, always compile these separately and include them into your code as graphics. Otherwise they are recompiled for each student, and this is computationally expensive. 

#### How-To use Auto-Multiple-Choice (AMC):

The directory is pre-configured for the AMC software. You can open it as an existing project. 

![](https://gitlab.math.ethz.ch/amc/howto/-/raw/master/images/open-project.png)

The software is documented [here](https://gitlab.math.ethz.ch/teaching/amc-howto). **Page will be updated soon!**

#### Randomization with TeX (if needed):

The template possesses the ability to randomize questions. **The explanation of how randomization works can be found [here](https://gitlab.math.ethz.ch/teaching/exam-template/-/wikis/Randomization-using-TeX)**. If you already know how it works use this [**tool to generate the files**](https://isg.math.ethz.ch/make-anonym-csv/index2.html).

*Note:* This type of randomization / versioning does not use the AMC software (but it is compatible with it!). The method using TeX is more elementary and completely reproducible. Alternatively you can use AMC. In this case, please follow the general format of the exam template (in particular, use the `separateanswersheet` and `automarks` option when loading the automultiplechoice package. 


## 2. Printing exam documents

Printing of personalized exam documents is automated. Please use the **D-MATH Exam Tool** for this purpose:

1. Compile the necessary documents to generate PDF files (if MC is present: `exam.pdf`, `answer_mc.pdf`, `answer_book.pdf` and `list_doorsigns.tex`, if no MC is present: `exam.pdf`, `answer_book.pdf` and `list_doorsigns.tex`). Remember to compile `answer_mc.pdf` using the auto-multiple-choice software and running [layout detection](https://gitlab.math.ethz.ch/teaching/exam-template/-/wikis/Layout-detection-in-AMC). 
2. Use the [**D-MATH exam print tool**](https://isg.math.ethz.ch/exam-print-tool/) (authentication required) to generate a configuration file for automatized printing: 
    * Enter the number of students exactly as appearing in the 'registration.csv' file you used for compiling the exam.
    * Upload the PDF files.
3. The tool will run a basic consistency check.
4. Download the `*.zip` file containing all supporting documents, the exam papers for each student as well as the "job definition" file `print-job.flist` used by the printing service. 
5. Send the zip file to Print & Publish via the **"Prüfungen D-MATH" option (do not use the regular exam printing option)**. You will receive the printed, stapled and pre-sorted exams, as well as the supporting documents. 

**Note:** For the printout to be correct, it must be ensured that the number of pages for all exams, answer booklets and MC answer sheets is, respectively, *the same per student*! This can be checked in the respective `xxx_pagecount.csv` files generated by the LaTeX templates. The tool will instruct the printer to split the documents in equal page intervals, and then print and sort them by student in the order (i) exam, (ii) MC answer sheet, (iii) answer booklet.

**Note:** Please **avoid sending personalized exam documents to Print and Publish by other means than the Print Tool**. All exams in the standard format can be handled by the tool. You should **not** use colored MC boxes, etc. and send separate print jobs. 
#### Exam Print Tool:
![](https://gitlab.math.ethz.ch/amc/howto/-/raw/master/images/print-tool.png)

#### Print & Publish "Prüfungen D-MATH" Option:
![](https://gitlab.math.ethz.ch/teaching/exam-template/-/wikis/uploads/81292160467e23098724b1ff49f9fcef/dmath_print.jpg)
# Organizing the exam

## 3. Communication with students

Inform students about the structure of the exam. The format of the exam is [documented for students centrally](https://www2.math.ethz.ch/exam-information/) under this link:

`https://www2.math.ethz.ch/exam-information/` 

Include this information in your eMail.

### Sample eMail
In addition to exam-specific information and the central link, you may wish to copy parts of the following standard eMail snippets. The eMail is sent via eDoz.

**German version:**

<details>
<summary>Beispiel eMail für Prüfungen mit multiple choice Teil.</summary>

Liebe Studierende,

für die Prüfung 

XXX am XXX, von XXX bis XXX Uhr 

sende ich Ihnen zusätzlich zu den Informationen auf der Vorlesungswebsite nachstehend folgende weiteren Informationen:

1/ Räume

2/ Prüfungsunterlagen und Tipp-Ex

3/ Prüfungseinsicht nach der Notenbekanntgabe.


***

1/ Räume:

Es wird **feste** Plätze und **keine** freie Platzwahl geben. Die Zuteilung in die Räume erfolgt nach dem Anfangsbuchstaben Ihres Nachnamens. 

XXX (A-G) 

XXX (H-Z) 

Die Räume werden ab 30 Minuten vor Prüfungsbeginn zugänglich sein. Bitte begeben Sie sich nicht vorher in die Räume!

2/ Prüfungsunterlagen und Tipp-Ex:

Für die Prüfung werden vorgefertigte personalisierte und anonymisierte Antworthefte benutzt, welche bereits auf dem Tisch liegen werden und Ihren Sitzplatz festlegen.

Ein Beispiel-Antwortheft mit Erklärung finden Sie hier:

	https://www2.math.ethz.ch/exam-information/

Bitte machen Sie sich damit vertraut, um Ihren Prüfungsplatz zügig zu finden.

hre Prüfung hat zusätzlich Single- oder Multiple-Choice-Fragen. Anweisungen zur Bearbeitung finden Sie auf der selben Website.
Zur Korrektur von Single- oder Multiple-Choice-Fragen benötigen Sie Tipp-Ex. Bitte bringen Sie welches mit! Die Prüfungsaufsicht wird Reserve bereitstellen.

Es werden **nur die Lösungen in den offiziellen bereitgestellten Antwortheften bewertet**! Bitte bringen Sie dennoch eigenes leeres Papier für Notizen und Nebenrechnungen mit.

3/ Prüfungseinsicht nach der Notenbekanntgabe

Lassen Sie mich abschliessend über die zentrale Prüfungseinsicht im Anschluss an die Notenkonferenz des Departements informieren: auf der Website XXX finden Sie alle relevanten Daten.
 
Viel Erfolg und freundliche Grüsse
 
XXX

</details>

**English version:**


<details>
<summary>Sample eMail for exam with multiple choice part.</summary>

Dear Students,

I hope you had a good start into the examination session. For the exam 

XXX on XXX from XXX to XXX

I am sending you additional information complementing those on the course website:

1/ Exam room

2/ Examination papers

3/ Exam review after the announcement of the grades

***

1/ Exam room:

Seats are **preassigned** based the initials of your name.

XXX (A-G)

XXX (H-Z)

The examination rooms will be accessible 30 minutes before the exam begins. Please do not enter the room before this time. 

2/ Examination papers:

Pre-printed personalized and anonymized examination papers will be used for the exam. You will find these distributed in the room at your preassigned seat. A sample answer-booklet with explanation can be found here: 

    https://www2.math.ethz.ch/exam-information/

Please acquaint yourself with the format to locate yours.

In addition the exam contains multiple choice questions. You will find instructions on how to answer such questions on the website. To change your response to such questions you will require white-out (Tipp-Ex). Please have some with you! The assistants supervising the exam will provide some backup.  

Only answers written in the provided exam documents will be graded! Please bring scrap paper for notes or calculations.

3/ Exam review after the announcement of the grades:

For information about the exam review, please consult the website 

XXX


Good luck and best regards

XXX 

</details>

*Coming soon: Tool to send personalized eMails to students that include their exam number.*
## 4. On the day of the exam

- In case the exam contains a MC part, pack additional white-out (Tipp-Ex) in case a student forgets theirs. 
- Bring additional blank paper for students. 
- Bring a stapler.
- Bring Tape for the door signs. 

## 5. Grading

The file `GradingWorkbookCreator.xlsm` is an Excel based grading file that communicates with eDoz (i.e. it is generated with data downloaded from eDoz and exports the final grades in a format accepted by eDoz as source).

### Inputing points and using the grading file

[**Wiki with instructions**](https://gitlab.math.ethz.ch/teaching/exam-template/-/wikis/Using-the-grading-file). Includes:
- Suggestions on how to input points during grading.
    - Directly while grading with a latop, by distributiing the `registration.csv` to graders.
    - Using AMC (example code provided).
- Import from, and export to eDoz using the grading file (Microsoft Excel based).
- Question specific, and overall exam statistics / histograms. 

![](https://gitlab.math.ethz.ch/teaching/exam-template/-/wikis/uploads/65efe308afe9700ba5254637abb3c308/Screenshot_from_2022-12-05_18-20-00.png)

### Statistical analysis of results

There is a tool that analyzes the performance of students statistically and generates a graphical overview, e.g. 

![](https://gitlab.math.ethz.ch/teaching/exam-template/-/wikis/uploads/14b15800d8c4751ab881ac02d4ec0fd6/kennzahlen-mc.png)

The tool will be made available in due time. Please check the [wiki](https://gitlab.math.ethz.ch/teaching/exam-template/-/wikis/Statistical-analysis-of-multiple-choice-results) for updates. 

