\vspace{0.2cm}

Sei $n\in \mathbb N \setminus \{0\}$ und $\Theta = \mathbb R$. Wir wählen eine Modellfamilie $(\mathbb P_\theta)_{\theta\in \mathbb R}$, sodass die Zufallsvariablen $X_1,\ldots X_n$ unter $\mathbb P_\theta$ unabhängig und identisch verteilt sind mit
\begin{equation*}
	X_1\sim \mathcal N(\theta,4). 
\end{equation*}
Der Parameter $\theta$ ist unbekannt und soll im Folgenden geschätzt werden.

\textbf{Erinnerung:} Aus $X_1 \sim \mathcal N(\theta,4)$ unter $\mathbb P_\theta$ folgt, dass $X_1$ stetig ist mit Dichte 
	\begin{equation*}
		f_{X_1}(x_1) = \frac{1}{\sqrt{8\pi}} e^{-\frac{1}{8}(x_1-\theta)^2}. 
\end{equation*}

\begin{enumerate}



\itemOpen \pointsA{3} Zeigen Sie, dass der Maximum-Likelihood Schätzer gegeben ist durch
\begin{equation*}
	\label{eq:11}
	T=T_{ML}=\frac1 n \sum_{i=1}^n X_i\,. 
\end{equation*}

\sol{
	Die Likelihood Funktion und die log-Likelihood Funktion sind gegeben durch:
	\begin{align*}
		&L(x_1,\ldots,x_n;\theta) = \frac{1}{(8\pi)^{n/2}} e^{-\frac{1}{8}\sum_{i=1}^n(x_i-\theta)^2}. \\
		&\log L(x_1,\ldots,x_n;\theta) = -\frac{n}{2}\log(8\pi) - \frac{1}{8} \sum_{i=1}^n(x_i-\theta)^2. \pointsM{1}
	\end{align*}
	Um das Maximum zu bestimmen, leiten wir die log-Likelihood Funktion nach $\theta$ ab:
	\begin{equation*}
		\frac{d}{d\theta} \log L(x_1,\ldots,x_n;\theta) = \frac{1}{4} \sum_{i=1}^n(x_i-\theta). \pointsM{1}
	\end{equation*}
	Es folgt, dass die log-Likelihood Funktion (und somit auch die Likelihood Funktion) ihr Maximum bei 
	\begin{equation*}
		\theta^\ast = \frac{1}{n} \sum_{i=1}^n x_i \pointsM{1}
	\end{equation*}
	annimmt, da die Ableitung für $\theta < \theta^\ast$ strikt positiv, an der Stelle $\theta^\ast$ gleich null und für $\theta > \theta^\ast$ strikt negativ ist. Hieraus folgt 
	\begin{equation*}
		T_{ML} = \frac{1}{n} \sum_{i=1}^n X_i\,.
	\end{equation*}
}


\itemOpen \pointsA{1} Ist der Schätzer $T_{ML}$ aus der vorherigen Teilaufgabe erwartungstreu?

\sol{Ja, der Schätzer $T_{ML}$ ist erwartungstreu, da für alle $\theta \in \mathbb R$ gilt 
	\begin{equation*}
		\mathbb E_\theta [T_{ML}] = \frac{1}{n} \sum_{i=1}^n \underbrace{\mathbb E_\theta[X_i]}_{=\theta} = \theta,
	\end{equation*}
	wobei wir die Linearität des Erwartungswertes verwendet haben.
}

\itemOpen \pointsA{2} Berechnen Sie den mittleren quadratischen Schätzfehler $\mathrm{MSE}_\theta[T_{ML}]$ für jedes $\theta\in \mathbb R$.

\sol{Berechnung der Varianz von $T_{ML}$ für jedes $\theta \in \mathbb R$:
	\begin{equation*}
		\mathrm{Var}_\theta[T_{ML}]  =  \frac{1}{n^2} \mathrm{Var}_\theta[\sum_{i=1}^n X_i ]  =  \frac{1}{n^2} \sum_{i=1}^n \underbrace{\mathrm{Var}_\theta[X_i]}_{=4}  = \frac{4}{n} \pointsM{1}
	\end{equation*}	
	
	
	Da der Schätzer $T_{ML}$ erwartungstreu ist, liefert die Zerlegung 
	\begin{equation*}
		\mathrm{MSE}_\theta[T_{ML}] = 	\mathrm{Var}_\theta[T_{ML}] + (\mathbb E_\theta[T_{ML}]-\theta)^2=  \frac{4}{n} + 0 = \frac{4}{n}. \pointsM{1}
	\end{equation*}
	
}

\itemOpen \pointsA{2} Zeigen Sie, dass für jedes $\theta \in \mathbb{R}$
\begin{equation*}
	\frac{\sqrt{n}\cdot  (T_{ML}-\theta)}{2} \sim \mathcal{N}(0,1) \quad \text{unter}\;  \mathbb P_\theta.
\end{equation*}

\sol{ 
	Aus der Vorlesung ist bekannt, dass $T_{ML} = \frac{1}{n} X_1 + \ldots + \frac{1}{n} X_n$ als Summe von unabhängigen normalverteilten Zufallsvariablen wieder normalverteilt ist mit Erwartungswert $$\frac{1}{n}\mathbb E[X_1] + \ldots \frac{1}{n} \mathbb E_\theta[X_n] = \theta$$ und Varianz $$\frac{1}{n^2} \mathrm{Var}_\theta[X_1] + \ldots + \frac{1}{n^2} \mathrm{Var}_\theta[X_n] = \frac{4}{n}.$$
	\textit{[Erwartungswert und Varianz sind bereits aus vorherigen Teilaufgaben bekannt.]}
	
	Es gilt also $T_{ML} \sim \mathcal{N}(\theta,4/n)$. \points{1}
	Durch Standardisieren erhält man  $$\frac{\sqrt{n}\cdot  (T_{ML}-\theta)}{2} \sim \mathcal{N}(0,1) \pointsM{1}. $$
	
	\textit{Alternativ:} Die Formel aus der Vorlesung für die Summe unabhängiger normalverteilter Zufallsvariablen kann auch direkt auf den Ausdruck
	\begin{equation*}
		-\frac{\sqrt{n}\cdot \theta}{2} + \frac{1}{2 \sqrt{n}} X_1 + \ldots + \frac{1}{2 \sqrt{n}} X_n
	\end{equation*}
	angewendet werden.
}


\itemOpen \pointsA{2} Bestimmen Sie ein Konfidenzintervalll für $\theta$ mit Niveau $95\%$. 

\textbf{Hinweis:} Für eine Zufallsvariable $Z\sim\mathcal N(0,1)$ gilt 
	\begin{equation*}
		\mathbb P[-1.96 \le Z \le 1.96] \ge 0.95.
	\end{equation*}


\sol{Aus der Tabelle erhalten wir, dass für eine $\mathcal{N}(0,1)$-verteilte Zufallsvariable $Z$ gilt
	\begin{equation*}
		\mathbb P_\theta[-1.96 \le Z \le 1.96] \ge 0.95. 
	\end{equation*}
	Aus Aufgabenteil (d) folgt also 
	\begin{align*}
		0.95 &\le \mathbb P_\theta\left[-1.96 \le \frac{\sqrt{n}\cdot  (T_{ML}-\theta)}{2} \le 1.96\right] \\
		&= \mathbb P_\theta\left[T_{ML}-\frac{3.92}{\sqrt{n}} \le \theta \le T_{ML} + \frac{3.92}{\sqrt{n}}\right].
		\pointsM{1}
	\end{align*}
	Ein Konfidenzintervall für $\theta$ mit Niveau $95\%$ ist also gegeben durch 
	\begin{equation*}
		I=\left[T_{ML}-\frac{3.92}{\sqrt{n}},T_{ML}+\frac{3.92}{\sqrt{n}}\right]. \pointsM{1}
	\end{equation*}
}










\end{enumerate}			
	
%\if\gensolution0
 %\newpage
%\fi