To generate csv, use either:

Method:
#1 Use the online tool https://isg.math.ethz.ch/make-anonym-csv/
#2 manually in excel.
#3 using python script.

ad #1

1/ go to edoz
2/ navigate to Communication/Lists > Performance assessments -
Registrations & Results > *your exam* > Export data (excel)
3/ Upload the file to https://isg.math.ethz.ch/make-anonym-csv/ (wait about 15 seconds for page to fully load)
4/ Doenload the `registration.csv`

ad 2/
Do steps 1 and 2 above, then follow the instructions below. Also check the example file "ExaminationRegistrations_xxxx.xlsx" in case you are unsure (or want to copy from there).

3/ In the excel file, delete all student information other than "Last_Name", "First_Name", "Number"
4/ Insert empty column on the left of the sheet. This column (column A) contains the exam number. Rename field A1 "id", Entries in A2, A3, A4,... should be running numbers 1, 2, 3, ...
4/ Rename field A2 from "Last_Name" to "Name"
5/ In field E1 use code "=LEFT(A1;2)"
6/ In field F1 use code "=LEFT(A2;2)"
7/ In field G1 use code "=REPLACE(C1; 1;2;"XX")"
8/ Rename field H1 to "RandomGroup",
9/ Fill H2 with v0 (this is the group of the student, if you have multiple versions [e.g. random order of questions], give students groups v1, v2)
10/ Rename field I1 to "Room"
11/ Fill the remaining fields in the I column with the exam hall for each student. 
12/ apply these to all rows
13/ Save file as "registration.csv" (; separated values, csv)

**REMARK: Some computers won't let you choose the delimiter and might set , instead of ; (in particular mac). Either do this on a linux computer with Libre Office, or use an online tool such as https://conversiontools.io/convert/excel-to-csv which lets you choose the delimiter when converting from xls to csv.

ad #3
-> check out the README.md file in the directory supportFiles/
